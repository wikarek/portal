<?php
    function wyswietlanie_link_user(){
        $database = include 'database_log.php';
        $host_dat = $database['host'];
        $name_dat = $database['database'];
        $user_dat = $database['user'];
        $pass_dat = $database['password'];
        $pol = new mysqli($host_dat, $user_dat, $pass_dat, $name_dat);
        if($pol->connect_error){
            die("nie udalo sie polaczyc ". $pol->connect_error);
        }
        else{
            $sql = "SELECT nazwa_linku FROM linki";
            $st = $pol->query($sql);
            $il_rek = 0;
            if ($st !== false){
                $il_rek = $st->num_rows;
            }
            $rek = [];
            if ($il_rek > 0){
                $rek = $st->fetch_all(MYSQLI_ASSOC);
            }
            return ['rekordy' => $rek];    
        }

    }
?>

<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta charset="utf-8"/>
        <title>Portal</title>
        <link rel="stylesheet" href="style_user.css" type="text/css"/>
    </head>
    <body>
        <div id="glowny">
            <div id="wybor_linku">
                <?php
                    $zaw_tabeli = wyswietlanie_link_user(); ?> 
                    <?php if(empty($zaw_tabeli['rekordy'])){
                        echo "tabela jest pusta";
                    }
                    else{?>
                           <?php foreach($zaw_tabeli['rekordy'] as $rek):?>
                                <?php foreach ($rek as $value): ?>
                                    <a href="#"  onclick='wyswietl("<?=$value; ?>")'><?=$value; ?></a>
                                <?php endforeach; ?>
                            <?php endforeach; } ?> 
            </div>
            <div id="wyswietl">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                <script>
                 function wyswietl(wartosc){     
                    $(document).ready(function(){
                    $('#iframe').attr('src', wartosc);
                    });
                } 
                </script>        
                    <iframe id="iframe" name="myIframe" frameborder="5" width="100%" height="100%"></iframe>
            </div>
        </div>
    </body>
</html>