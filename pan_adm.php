<?php
    $dat = include 'database_log.php';
    $host_dat = $dat['host'];
    $name_dat = $dat['database'];
    $user_dat = $dat['user'];
    $pass_dat = $dat['password'];
    $pol = new mysqli($host_dat,$user_dat,$pass_dat);
    if($pol -> connect_error){
        echo "Nie udalo sie polaczyc z baza " . $pol -> connect_error;
    }
    if(isset($_GET['dodaj_baze'])){
        $sql_query  =  'CREATE DATABASE IF NOT EXISTS portal_lab5';
        if($pol->query($sql_query) === TRUE){
            header('Location: /portal/pan_adm.php');
        }
        else{
            die("nie udalo sie utworzyc bazy danych \n". $pol->error);
        }
        $pol->close();
    }

    if(isset($_GET['dodaj_tabele'])){
        $pol = new mysqli($host_dat,$user_dat,$pass_dat, $name_dat);
        if($pol->connect_error){
            die("nie udalo sie polaczyc ". $pol->connect_error);
        }else{
            $sql  = "
            CREATE TABLE IF NOT EXISTS linki(
                id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                nazwa_linku varchar(100) not null ,
                typ_linku  varchar(30) not null
            )";
            if($pol->query($sql) === TRUE){
               $sql = 'INSERT INTO linki (nazwa_linku, typ_linku) Values ("http://dynamicdrive.com/dynamicindex17/indexb.html", "zewnetrzny")';
               $pol->query($sql);
               $sql = 'INSERT INTO linki (nazwa_linku, typ_linku) Values ("log.php", "wewnetrzny")';
               $pol->query($sql);
               header('Location: /portal/pan_adm.php');
            }
            else{
                die("nie udalo sie utworzyc tabeli  ". $pol->error);
            }
            $pol->close();
        }
    }

    if(isset($_GET['usun_tabele'])){
        $pol = new mysqli($host_dat, $user_dat, $pass_dat, $name_dat);
        if($pol->connect_error){
            die("nie udalo sie polaczyc ". $conn->connect_error);
        }else{
            $sql  =  "DROP TABLE IF EXISTS linki";
            if($pol->query($sql) === TRUE){
                header('Location: /portal/pan_adm.php');
            }
            else{
                echo ("nie udalo sie usunac tabeli ". $pol->error);
            }
            $pol->close();
        }      
    }

    if(isset($_GET['usun_baze'])){
        $pol = new mysqli($host_dat, $user_dat, $pass_dat, $name_dat);
        if($pol->connect_error){
            die("nie udalo sie polaczyc ". $pol->connect_error);
        }else{
            $sql_query  =  'DROP DATABASE IF EXISTS portal_lab5';
            if($pol->query($sql_query) === TRUE){
                header('Location: /portal/pan_adm.php');
            }
            else{
                die ("nie udalo sie usunac bazy danych ". $pol->error);
            }
            $pol->close();
        }
    }

    if(isset($_POST['wstaw_link'])){
        $pol = new mysqli($host_dat, $user_dat, $pass_dat, $name_dat);
        if($pol->connect_error){
            die("nie udalo sie polaczyc ". $pol->connect_error);
        }else{
            $link = (string)htmlspecialchars($pol->real_escape_string($_POST['link']));
            $typ_link = $_POST['typ_link'];
            $sql_query = 'INSERT INTO linki (nazwa_linku, typ_linku) Values (?, ?)';
            $stm = $pol->prepare($sql_query);
            $stm->bind_param('ss', $link, $typ_link);
            if ($stm->execute() === false) {
                echo('Rekord nie został dodany'. $pol->error);
            }
            else{
                echo "Dodano nowy rekord do tabeli";
            }
            $pol->close();    
        }
    }

    if(isset($_POST['usun_link'])){
        $pol = new mysqli($host_dat, $user_dat, $pass_dat, $name_dat);
        if($pol->connect_error){
            die("nie udalo sie polaczyc ". $pol->connect_error);
        }else{
            $id = (int)htmlspecialchars($pol->real_escape_string($_POST['id']));
            if (!is_numeric($id)) {
                echo('Rekord nie został usunięty, id powinno byc liczba ');
            }else{
                $sql= " DELETE FROM linki WHERE id = ?";
                $st = $pol->prepare($sql);
                $st->bind_param('i', $id);
                if ($st->execute() === false){
                    die("podane id nie istnieje");
                }else{
                    echo "rekord zostal usuniety";
                }   
                $pol->close();
            }    
        }
    }

    function wys_link_panel($pol){
        $sql = 'SELECT id, nazwa_linku, typ_linku FROM linki';
        $st = $pol->query($sql);
        $il_rek = 0;
        if ($st !== false){
            $il_rek = $st->num_rows;
        }
        $rekordy = [];
        if ($il_rek > 0){
            $rek = $st->fetch_all(MYSQLI_ASSOC);
        }
        return ['rekordy' => $rek];

    }

    if(isset($_POST['wyloguj'])){
        setcookie("admin_zalogowany", "", time() - 3600);
        header('Location: log.php');
    }
?>

<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta charset="utf-8"/>
        <title>Portal</title>
        <link rel="stylesheet" href="style_panel.css" type="text/css"/>
    </head>
    <body>
        <?php
            if(!isset($_COOKIE['admin_zalogowany'])) {
                echo "Nie jestes zalogowany";
                header('Location: log.php');
            }else{
          
        ?>
        <div id="admin_panel" >
            <div id="user" >
                <h5>Zalogowany uzytkownik: <? echo $_COOKIE['admin_zalogowany']; ?></h5>
                <form method="POST" action="pan_adm.php" >
                    <input type="submit"  value="Wyloguj" name="wyloguj" />
                </form>
            </div>
            <div id="menu" >
                <a href="/portal/pan_adm.php/?dodaj_baze" class="przycisk" >Dodaj Baze danych </a>
                <a href="/portal/pan_adm.php/?dodaj_tabele" class="przycisk" >Dodaj Tabele </a>
                <a href="/portal/pan_adm.php/?usun_tabele" class="przycisk" >Usun Tabele </a>
                <a href="/portal/pan_adm.php/?usun_baze" class="przycisk" >Usun Baze danych </a>
            </div>
            <div id="zawartosc">
                <h4>Dodawanie rekordów</h4>
                <form method="POST" action="pan_adm.php">
                    <label for="link">Link: </label>
                    <input type="text" name="link" id="link" required/>
                    <table>
                        <tr>
                            <td> <label for="typ_link"></label>Link zewnetrzny:</td>
                            <td> <input type="radio" id="link_zew" name="typ_link" value="zewnetrzny" checked/></td>
                            <td> <label for="typ_link">Link wewnetrzny:</label> </td>
                            <td> <input type="radio" id="link_wew" name="typ_link" value="wewnetrzny"/></td>
                        </tr>
                        <tr>
                            <td> <input type="submit" value="Dodaj rekord"  name="wstaw_link"/></td>
                        </tr>
                    <table>
                </form>
                <h4>Usuwanie rekordów: </h4>
                <table>
                <form method="POST" action=""> 
                    <tr>
                        <td> <label for="id">Podaj Id: </label></td>
                        <td> <input type="text" name="id" id="id" required> </td>
                    </tr>
                    <tr>
                        <td> <input type="submit" value="Usun rekord"  name="usun_link"/> </td>
                    </tr>
                </form>
                </table>
                <h4>Wyswietlanie rekordów: </h4>
                <?php
                    $pol = new mysqli($host_dat, $user_dat, $pass_dat, $name_dat);
                    if($pol->connect_error){
                        die("nie udalo sie polaczyc ". $pol->connect_error);
                    }
                    $zawartosc_tabeli = wys_link_panel($pol); ?> 
                    <?php if(empty($zawartosc_tabeli['rekordy'])){
                        echo "tabela jest pusta";
                    }
                    else{?>
                        <table class="tabela_adm">
                        <tr>
                            <th>id</th> <th>link</th>  <th>typ_linku</th> 
                        </tr>
                        <tr>
                        <?php foreach($zawartosc_tabeli['rekordy'] as $rekordy):?>
                        <tr>
                            <?php foreach ($rekordy as $wartosc): ?>
                                <td><?=$wartosc; ?></td>
                            <?php endforeach; ?>
                        </tr>
                        <?php endforeach; } ?>
                        </table>    
            </div>
        </div>
        <? } ?>
    </body>
</html>